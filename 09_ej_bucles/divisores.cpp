
#include <stdio.h>
#include <stdlib.h>

int main(){
    int num;

    printf("Dime un numero: ");
    scanf(" %i", &num);
    system("clear");
    printf("Estos son los divisores de %i: \n\n", num);
    for(int n=2; n<num; n++){
        if(num%n==0)
            printf("Divisor %i \n",n);
    }
    printf(" \n");

    return EXIT_SUCCESS;
}
