
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int primo(int numero){
    int cont=0;
    for(int num=1; num <= numero; num++)
        if (numero % num == 0){
            cont++;
        }
        if (cont==2)
            printf("Es primo \n");

        else
            printf("No es primo \n");
}

//Funcion punto de entrada
int main(){
    int  numero;

    printf("Vamos a comprobar si tu numero es primo o no\nEscribe el numero: ");
    scanf("%i", &numero);

    primo(numero);

    return EXIT_SUCCESS;
}
