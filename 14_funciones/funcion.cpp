
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 10


void num(int op[N]){
    for(int i=0; i<N; i++)
        op[i] = pow(i,2);
}

//Funcion punto de entrada
int main(){
   int x[N];

   num(x);

   for(int i=0; i<10; i++)
        printf(" En la posicion x[%i] la potencia es %i \n", i, x[i]);

    return EXIT_SUCCESS;
}
