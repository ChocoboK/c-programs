
#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "snake.h"

void pintar (struct TSnake snake){
    clear(); // Limpia pantalla
    for (int i=0; i<snake.cima; i++)
        mvprintw (snake.anillo[i].pos.y,
                snake.anillo[i].pos.x, "O");
    refresh();
}

//Funcion punto de entrada
int main(int argc, char *argv[]){
    struct TSnake snake;
    int input;

    initscr ();
    halfdelay(2);
    keypad(stdscr, TRUE);
    curs_set(0);
    iniciar(LINES, COLS);

    parir (&snake);


    do{
        // Mirar entrada
        input = getch ();
        if (input >=KEY_DOWN && input <= KEY_RIGHT)
            snake.anillo[0].vel = velocidades[input - KEY_DOWN];
        //if (snake.anillo[0].pos.y = 0)
        //     return KEY_UP;
        //    snake.anillo[0].pos.y += 1;
        //    snake.anillo[0].pos.x += 1;
        // Actualizar fisica
        // mvprintw (snake.pos.y, snake.pos.x, " "); // Double buffer (no funciona ya)
        mover (&snake);
        // Limite de pantalla por columnas
        if (snake.anillo[0].pos.x > COLS)
            snake.anillo[0].pos.x = 0;
        else if (snake.anillo[0].pos.x < COLS - COLS)
                 snake.anillo[0].pos.x = COLS;
        // Limite de pantalla por lines
        if (snake.anillo[0].pos.y > LINES)
            snake.anillo[0].pos.y = 0;
        else if (snake.anillo[0].pos.y < LINES - LINES)
                 snake.anillo[0].pos.y = LINES;
        // Repintar
        pintar (snake);
    } while(input != 0x1B);

    curs_set(1);
    endwin();

    return EXIT_SUCCESS;
}
