
#include <stdio.h>
#include <stdlib.h>

int main(){
    unsigned primo[] = {2, 3, 5, 7, 11, 13, 17, 19, 23};
    unsigned /*long*/ elementos = (unsigned) sizeof(primo) / sizeof(int);
    unsigned *peeping  = primo;
    char *tom = (char *) primo;
    unsigned **police = &peeping; //esto contiene la direccion de la direccion de un puntero.
    unsigned (*police2)[9] = primo;
    // unsigned *plc[9] = primo; //9 celdas cada una con unsigned *


    printf("PRIMO:\n"
            "======\n"
            "Localizacion (%p)\n"
            "Elementos: %u [%u...%u]\n"
            "Tamaño: %lu bytes. \n\n",
            primo,
            elementos,
            primo[0], primo[elementos -1],
            sizeof(primo));
    printf(" 0: %u \n", peeping[0] );
    printf(" 1: %u \n", peeping[1] );//anotacion de matrices
    printf(" 0: %u \n", *peeping ); // * = alli donde apunta "peeping"
    printf(" 1: %u \n", *(peeping+1)); //anotacion de puntero
    printf(" Tamaño: %lu bytes. \n", sizeof(peeping));
    printf(" \n ");

    printf("Como se ve en memoria \n");
    for (int i=0; i<sizeof(primo); i++)
        printf("%02X", *(tom+i));
    printf(" \n\n ");

    printf("Police contiene: %p\n", police);
    printf("Primo contiene: %p\n", *police);
    printf("Primo[0] contiene: %u\n", **police); //See leeria: police contiene alli donde apunta (peeping en este caso) que es alli donde apunta police.


    return EXIT_SUCCESS;
}
