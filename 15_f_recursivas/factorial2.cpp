
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//Funcion que calculara
double factorial(double n, double p){
    if(p==1)
        return n;
    return n + (1 / factorial(n, p-1));
}

//Funcion punto de entrada
int main(int argc, char *argv[]){
    double n,
           p;

    //Recogida de datos
    printf("Dime un numero: ");
    scanf(" %lf", &n);
    printf("Dime la profundidad: ");
    scanf(" %lf", &p);

    //Resultado imprimido
    printf("%lf \n", factorial(n,p));

	return EXIT_SUCCESS;
}
