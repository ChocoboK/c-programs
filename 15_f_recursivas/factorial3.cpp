

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define E 0.000001

//Funcion que calculara
double factorial(double n, double p){
    if(p==1)
        return n;
    return n + (1 / factorial(n, p-1));
}

//Funcion punto de entrada
int main(int argc, char *argv[]){
    double n,
           res0 = 0,
           res1 = -100;
    int i;

    //Recogida de datos
    printf("Dime un numero: ");
    scanf(" %lf", &n);

    //Llamada a la funcion
    for (i=1; fabs(res1-res0) > E && i<20; i++){
        res0 = res1;
        res1 = factorial(n,i);
    }

    //Resultado imprimido
    printf("FC de (%.lf, %i) es = %lf \n", n, i, res1);

	return EXIT_SUCCESS;
}
