
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int mul(int n){
    if (n==0)
        return 1;
    return n* mul(n-1);

}

//Funcion punto de entrada
int main(int argc, char *argv[]){
    int n;

    printf("Dime un numero: ");
    scanf(" %i", &n);

    printf("%i \n", mul(n));

	return EXIT_SUCCESS;
}
