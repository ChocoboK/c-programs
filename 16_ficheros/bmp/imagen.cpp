
#include <stdio.h>
#include <stdlib.h>

#define BMPNAME "paisaje.bmp"
#define WIDTH 450L
#define HEIGHT 289L
#define Bpp 3
#define ROW_SIZE (WIDTH * HEIGHT + 3) / 4 * 4

#define IMAGE_OFFSET 0x8a

    //abrir un fichero
    //Avanzar hasta la posicion 8a
    //leer 450 x 289 x 3 --> los pixeles que ocupa tu imagen
    //cerrar el fichero

//Funcion punto de entrada
int main(int argc, char *argv[]){
    unsigned char *image; // un byte es un char
    image = (unsigned char *) malloc(HEIGHT * ROW_SIZE);//malloc reservara la memoria que yo quiera reservar, devolvera la direccion de memoria (los bytes que quiera reservar)
    FILE *pf;

    if ( !(pf = fopen(BMPNAME, "r")) ){
        fprintf(stderr, "Mac donde estas?\nRai no te veo.\n");
        return EXIT_FAILURE;
    }
    fseek(pf, IMAGE_OFFSET, SEEK_SET);
    fread(image, 1, HEIGHT * ROW_SIZE, pf);
    fclose(pf);

    for(int row=0; row<HEIGHT; row++) {
        for (int col=0; col<600; col+=3) {
            double media = 0;
            for (int i=0; i<Bpp; i++)
                media += image[ROW_SIZE * row + Bpp * col + i];
            media /= 3;
            if (media > 120)
                printf("*");
            else
                printf("-");
        }
        printf("\n");
    }
    free (image);
    return EXIT_SUCCESS;
}
