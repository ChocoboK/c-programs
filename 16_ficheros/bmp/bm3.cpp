
#include <stdio.h>
#include <stdlib.h>

#define BMP_NAME "actress.bmp"
// #define WIDTH 1192L
// #define HEIGHT 984L
#define WIDTH 1192L
#define HEIGHT 984L
#define Bpp   3
#define ROW_SIZE (WIDTH * Bpp + 3 ) / 4 * 4

//#define IMAGE_OFFSET 0x8A
#define IMAGE_OFFSET 0x36

const char *two[] = { "█" , "░" };

const char *two_colors (unsigned char b, unsigned char w) {
    int media = (b + w) / 2;
    return two[media / 127];
}

void imprimir(unsigned char *image, int valim){
    for (int row=HEIGHT; row>=0; row-- ){
        for (int col=0; col<WIDTH; col++) {
            unsigned char * px = &image[ROW_SIZE * row + Bpp * col];
            printf ("%s", two_colors(
                        px[0],
                        px[1]
                        ));
        }
    printf ("\n");
    }
}
int  main(int argc, char *argv[]){
    unsigned char *image;
    char *img;
    image = (unsigned char *) malloc( HEIGHT * ROW_SIZE ) ;

    FILE *pf;
    printf("Nombre de la imagen: ");
    scanf("%s", img);

    if (!(pf = fopen (img, "r"))){
        fprintf (stderr, "Mac donde estas?\nRai no te veo.\n");
        return EXIT_FAILURE;
    }

    fseek (pf, IMAGE_OFFSET, SEEK_SET);
    fread (image, 1, HEIGHT * ROW_SIZE, pf);
    fclose (pf);

    imprimir(image, 127);

    free (image);
    return EXIT_SUCCESS;
}
