
#include <stdio.h>
#include <stdlib.h>

#define FileName "fibonacci.dat"

//Funcion punto de entrada
int main(int argc, char *argv[]){

    int sucesion[] = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89};
    FILE *pf;

    if ( !(pf= fopen(FileName, "wb")) ){
        fprintf(stderr, "Ajin. \n");
    }
    fwrite(sucesion, sizeof(int), sizeof(sucesion) / sizeof(int), pf);
    fclose(pf);

    return 0;
	//return EXIT_SUCCESS;
}
