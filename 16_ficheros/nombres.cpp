
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define filename "nombres.txt"
#define MAX 0x100
#define N 5

int main(int argc, char *argv[]){
    char nombre[N][MAX];
    FILE *pf;

    //pregunta los datos a guardar
    for(int i=0; i<N; i++){
        printf("Escribe un nombre a guardar: \n");
        fgets(nombre[i], MAX, stdin);
    }

    //esto guardara los datos anteriores en nombres.txt
    if ( !(pf = fopen (filename, "w")) )
        fprintf (stderr, "Couldn't find your %s\n", nombre[MAX]);
    for(int i=0; i<N; i++)
        fprintf(pf, " %s\n", nombre[i]);

    fclose (pf);

    return EXIT_SUCCESS;
}
