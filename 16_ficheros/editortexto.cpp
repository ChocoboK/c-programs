
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NOMBRE "algo.txt"
#define LETRAS 10
#define VECES 5

//Funcion punto de entrada
int main(int argc, char *argv[]){
    FILE *fichero;
    char *c[LETRAS];

    if ( !(fichero = fopen (NOMBRE,"w"))/* Tambien se podria quitar la ! y poner aqui un NULL*/)
        return EXIT_FAILURE;

    for (int i=0; i<VECES; i++){
        scanf(" %s", c[LETRAS]);
        fprintf(fichero, "%s\n", c[LETRAS]);
    }
    //getchar();
    fclose (fichero);

	return EXIT_SUCCESS;
}
