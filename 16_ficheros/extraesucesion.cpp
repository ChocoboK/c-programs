
#include <stdio.h>
#include <stdlib.h>

#define FileName "fibonacci.dat"
#define N 11

int rellenar (int suce[N], int i){
    if (i == 1){
        suce[0] = 1;
        return suce[1] = 1;
    }
    return suce[i] = rellenar (suce, i-1) + suce[i-2];
}

//Funcion punto de entrada
int main(int argc, char *argv[]){

    int sucesion[N];
    FILE *pf;

    rellenar (sucesion, N-1);

    if ( !(pf= fopen(FileName, "rb")) ){
        fprintf(stderr, "Ajin. \n");
        return EXIT_FAILURE;
    }
    fclose(pf);

	return EXIT_SUCCESS;
}
