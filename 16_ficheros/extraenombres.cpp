
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define filename "nombres.txt"
#define MAX 0x100
#define N 10

int main(int argc, char *argv[]){
    char nombre[N][MAX];
    FILE *pf;

    //esto leera los datos de nombres.txt
    if ( !(pf = fopen (filename, "r")) )
        fprintf (stderr, "Couldn't find your %s\n", nombre[MAX]);
    for(int i=0; i<N; i++)
        fgets(nombre[i], MAX, pf);

    fclose (pf);

    for(int i=0; i<N; i++)
        printf(" %s", nombre[i]);
    printf("\n");

    return EXIT_SUCCESS;
}
