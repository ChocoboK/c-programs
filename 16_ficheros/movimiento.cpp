
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define Filename "nombres.txt"

//Funcion punto de entrada
int main(int argc, char *argv[]){
    FILE *pf;
    int pos;

    srand (time (NULL));
    if( !(pf = fopen (Filename, "r")) ){
       fprintf(stderr, "Ain. \n");
       return EXIT_FAILURE;
    }
    pos = rand() % 100;

    fseek (pf, pos, SEEK_SET);
    printf("Pos: %i => %c \n", pos, (char) getc (pf));

    fclose (pf);
	return EXIT_SUCCESS;
}
