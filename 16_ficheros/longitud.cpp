
#include <stdio.h>
#include <stdlib.h>

#define Filename "nombres.txt"

//Funcion punto de entrada
int main(int argc, char *argv[]){
    FILE *pf;
    long int principio, final, longitud;

    //abrira el fichero de nombres.txt para leerlo.
    if( !(pf = fopen (Filename, "r")) ){
       fprintf(stderr, "Ain. \n");
       return EXIT_FAILURE;
    }

    //cogera los parametros del fichero: principio por donde empieza, final donde acabara, y longitud es la resta de los dos, que dara lo mismo que final.
    principio= ftell (pf);
    fseek (pf, 0, SEEK_END); //comienzo de fichero (pf), 0 sera la posicional final del archivo
    final= ftell (pf);
    longitud= final - principio;
    printf("Comienzo: %li \n"
           "Fin: %li \n"
           "Longitud: %li \n",
           principio, final, longitud);

    fclose (pf);

	return EXIT_SUCCESS;
}
