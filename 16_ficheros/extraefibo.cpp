
#include <stdio.h>
#include <stdlib.h>

#define FileName "fibonacci.dat"
#define N 11

//Funcion punto de entrada
int main(int argc, char *argv[]){

    int sucesion[N];
    FILE *pf;

    if ( !(pf= fopen(FileName, "rb")) ){
        fprintf(stderr, "ARRGH! \n");
        return EXIT_FAILURE;
    }
    fread(sucesion, sizeof(int), N, pf);
    fclose(pf);

    for(int i=0; i<N; i++)
        printf("%i ", sucesion[i]);

    printf("\n");

    return EXIT_SUCCESS;
}
