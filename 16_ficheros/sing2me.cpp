
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//Funcion punto de entrada
int main(int argc, char *argv[]){

    const char * cancion;
    char c;
    FILE *pf;

    if (argc < 2)
        return EXIT_FAILURE;

    cancion  = argv[1];
    if ( !(pf = fopen (cancion, "r")) )
        return EXIT_FAILURE;

    while ( (c = getc (pf)) != EOF)//EOF es final del fichero o error
        printf("%c",c);

    fclose (pf);

    return EXIT_SUCCESS;
}
