
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NOMBRE "cancion.txt"

//song contiene la direccion del primer salto de linea y al ser const no puede cambiar la direccion
const char * song = "\n\
                H\n\
                o\n\
                l\n\
                a\n\
\n\
                c\n\
                a\n\
                r\n\
                a\n\
                c\n\
                o\n\
                l\n\
                a\n\
                ";
void print_usage(){
    printf("Esto se usa asi\n");
}

void informo(const char *mssg){
    print_usage();
    fprintf(stderr, "%s\n", mssg);
    exit(1);
}

//Funcion punto de entrada
int main(int argc, char *argv[]){
    FILE *fichero;

    if ( !(fichero = fopen (NOMBRE, "w"))/* Tambien se podria quitar la ! y poner aqui un NULL*/)
        informo("No se ha podido abrir el fichero.");
    fprintf(fichero, "%s\n", song);
    getchar();
    fclose (fichero);

	return EXIT_SUCCESS;
}
