
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NOMBRE "algo2.txt"
#define LETRAS 0x100
#define VECES 10

//Funcion punto de entrada
int main(int argc, char *argv[]){
    FILE *fichero;
    char c[LETRAS];

    if ( !(fichero = fopen (NOMBRE,"w")) ){
        return EXIT_FAILURE;
    }

    printf("Dime %i frases.\n", VECES);

    for (int i=0; i<VECES; i++){
        printf("Frase: ");
        fgets(c, LETRAS, stdin);
        fprintf(fichero,"%s\n",c);
    };
    fclose (fichero);
	return EXIT_SUCCESS;
}
