
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define fichero "text.txt"

//Funcion punto de entrada
int main(int argc, char *argv[]){

    int veces['z' - 'a' + 1];
    FILE *pf;
    char *texto;

    if ( !(pf = fopen (fichero, "rb")) ){
        fprintf(stderr,"No se encuentra...");
        return EXIT_FAILURE;
    }

    printf("Tamaño: %li \n", sizeof(pf));

    texto = (char *) malloc(sizeof(veces));

    fread(veces, sizeof(int), sizeof(texto), pf);

    for(char *p=texto; *p!='\0'; p++){
        *p += 3;
    }

    printf("Tu texto: \n %c \n", texto[2]);

    fclose(pf);
    free(texto);

    return EXIT_SUCCESS;
}
