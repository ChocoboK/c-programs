
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define file "text.txt"

int main(int argc, char *argv[]){

    FILE *pf;
    char *text;
    int c;

    if ( !(pf = fopen (file, "rb")) ) {
        fprintf(stderr, "Arggg, nope!\n");
        return EXIT_FAILURE;
    }

    printf("Que buscas, que encuentras: \n");
    scanf(" %ms", &text);

    printf("Pues estoy buscando esto: %s\n\n", text);

    do {
        c = getc(pf);
        if( c == text[0] || tolower(c) == text[0] ) {
            if( strlen(text) == 1 )
                printf("Esta cosa que buscas: %c esta en %li\n", c, ftell(pf));
            else
                for(int i=1; i<strlen(text); i++) {
                    if( getc(pf) != text[i] ) break;
                    if( i == strlen(text ) - 1)
                        printf("Esta palabra, %s, esta en %li\n", text, ftell(pf) - strlen(text)+1);
                }
        }
    }while (c != EOF);

    return EXIT_SUCCESS;
}

