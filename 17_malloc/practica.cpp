
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 0x100

//Funcion punto de entrada
int main(int argc, char *argv[]){
    //preguntar una frase, guardarla en memoria (con %ms) y cambiar las letras de la frase por una del abcedario +3
    char *frase;

    printf("Escribe una frase: ");
    scanf(" %m[^\n]", &frase);

    for(char *p=frase; *p!='\0'; p++){
        *p += 3;
    }

    printf("Tu frase: \n %s \n", frase);


    free(frase);

	return EXIT_SUCCESS;
}
