
#include <stdio.h>
#include <stdlib.h>

#define RED 1
#define YEL 2
#define BLU 4

int main(){
    char answer;
    int color = 0;

    printf("¿Ves rojo? (s/n) \n");
    scanf(" %c", &answer);
    if (answer == 's')
        color |= RED;

    printf("¿Ves amarillo? (s/n) \n");
    scanf(" %c", &answer);
    if (answer == 's')
        color |= YEL;

    printf("¿Ves azul? (s/n) \n");
    scanf(" %c", &answer);
    if (answer == 's')
        color |= BLU;

    switch(color) {
        case 0:
            printf("Ves negro \n");
            break;
        case 1:
            printf("Ves rojo \n");
            break;
        case 2:
            printf("Ves amarillo \n");
            break;
        case 3:
            printf("Ves naranja \n");
            break;
        case 4:
            printf("Ves azul \n");
            break;
        case 5:
            printf("Ves morado \n");
            break;
        case 6:
            printf("Ves verde \n");
            break;
        case 7:
            printf("Ves blanco \n");
            break;
    }

	return EXIT_SUCCESS;
}
