
#include <stdio.h>
#include <stdlib.h>

#define ROJO 4
#define AMAR 2
#define AZUL 1

int main(){
    char respuesta;
    int color = 0;

    printf("¿Ves rojo? (s/N) \n");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= ROJO;

    printf("¿Ves amarillo? (s/N)\n");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= AMAR;

    printf("¿Ves azul? (s/N) \n");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= AZUL;

    switch (color) {
        case 0:
            printf("Ves Negro. \n");
            break;
        case 1:
            printf("Ves Azul. \n");
            break;
        case 2:
            printf("Ves Amarillo. \n");
            break;
        case 3:
            printf("Ves Verde. \n");
            break;
        case 4:
            printf("Ves Rojo. \n");
            break;
        case 5:
            printf("Ves Morado. \n");
            break;
        case 6:
            printf("Ves Naranja. \n");
            break;
        case 7:
            printf("Ves Blanco. \n");
            break;
    }

    return EXIT_SUCCESS;
}

