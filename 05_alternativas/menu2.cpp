
#include <stdio.h>
#include <stdlib.h>

#define CIR 1
#define CUA 2
#define TRI 4

int main(){
    char figura;
    int figura2 = 0;

    printf("¿Quieres un Circulo? (s/n) \n");
    scanf(" %c", &figura);
    if (figura == 's')
        figura2 |= CIR;

    printf("¿Quieres un Cuadrado? (s/n) \n");
    scanf(" %c", &figura);
    if (figura == 's')
        figura2 |= CUA;

    printf("¿Quieres un Triangulo? (s/n) \n");
    scanf(" %c", &figura);
    if (figura == 's')
        figura2 |= TRI;

    switch(figura2){
        case 0:
            printf("Pues te quedas sin figura \n");
            break;
        case 1:
            printf(" ○ \n");
            break;
        case 2:
            printf(" □ \n");
            break;
        case 3:
            printf("Dos son multitud. Elige solo una.\n");
        case 4:
            printf(" ▲ \n");
        case 5:
            printf("Dos son multitud. Elige solo una.\n");
        case 6:
            printf("Dos son multitud. Elige solo una.\n");
        case 7:
            printf("Demasiadas figuras quieres tu, eh.\n TOMAAAAA TIO ○  □  ▲ \n");
    }
    return EXIT_SUCCESS;
}
