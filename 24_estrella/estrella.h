#ifndef __ESTRELLA_H__
#define __ESTRELLA_H__

#define N 0x100

struct TEstrella {
    double ascension;
    double declinacion;
    double distancia;
    double diametro;
    double masa;
    double edad;
    double brillo;
};

struct TPila {
    struct TEstrella *data[N];
    int cima;
};

#endif

