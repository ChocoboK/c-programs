#include <stdio.h>
#include <stdlib.h>
#include "estrella.h"

#define file "estrella.txt"
#define MAX 0x100

int titulo(){
    char respuesta;
    system ("clear");
    printf("¿Quieres una estrella? (s/n): ");
    scanf(" %c", &respuesta);
    if(respuesta == 's'){
        return 1;
    }
    else if(respuesta == 'n')
        return 0;
    else
        titulo();
    return 0;
}

void push (struct TPila *data){
    data->cima++;
}


/* Función punto de entrada */
int main(int argc, char *argv[]) {
    struct TPila pila;
    pila.cima = 0;

    while( titulo() ){
        pila.data[pila.cima] = (struct TEstrella *) malloc(MAX * sizeof (struct TEstrella));
        printf("Datos de la estrella\n");
        printf("Dime ascension: ");
        scanf(" %lf", &pila.data[pila.cima]->ascension);
        printf("Dime declinación: ");
        scanf(" %lf", &pila.data[pila.cima]->declinacion);
        printf("Dime distancia: ");
        scanf(" %lf", &pila.data[pila.cima]->distancia);
        printf("Dime masa: ");
        scanf(" %lf", &pila.data[pila.cima]->masa);
        printf("Dime diametro: ");
        scanf(" %lf", &pila.data[pila.cima]->diametro);
        printf("Dime edad: ");
        scanf(" %lf", &pila.data[pila.cima]->edad);
        printf("Dime un brillo entre 0 a 6: ");
        scanf(" %lf", &pila.data[pila.cima]->brillo);
        push(&pila);
    }

    FILE *pf;

    if(!(pf = fopen(file, "w"))){
        fprintf(stderr,"error al abrir el archivo");
        return EXIT_FAILURE;
    }

    for (int i=0; i<pila.cima; i++){
        fprintf(pf, " ascension = %2.lf \n declinacion = %2.lf \n distancia = %2.lf \n masa = %2.lf \n diametro = %2.lf \n edad = %2.lf \n brillo = %2.lf \n\n",
                pila.data[i]->ascension, pila.data[i]->declinacion, pila.data[i]->distancia, pila.data[i]->masa,
                pila.data[i]->diametro, pila.data[i]->edad, pila.data[i]->brillo);

    }

    for(int i=0; i < pila.cima; i++)
        free(pila.data[i]);

    fclose(pf);

    return EXIT_SUCCESS;
}

