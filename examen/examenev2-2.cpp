
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX 10

//Funcion punto de entrada
int main(){
    int potencia  = 5;
    double f = 0, resultado = 1; // f de funcion
    int x = 2;

    double c/*polinomio*/[MAX] = {1, 2, 3, 4, 5}; //las potencias a calcular

    /*
    1·x^0 + 2·x^1 + 3·x^2 + 4·x^3 + 5·x^4
    c[0]·x^0 + c[1]·x^1 + c[2]·x^2 + c[3]·x^3 + c[4]·x^4
    c[i]·x^i                                                  <--- este comentario es el calculo a "papel" del for de termino de abajo
    f += c[i]·x^i                                                  la i es termino en el for
    */

    for(int termino=0; termino<MAX; termino++)
        f += c[termino] * pow(x, termino);

    /*
    for(int vez=0; vez<potencia; vez++)
        resultado *= x;
    */

    printf("f(%i) = %.2lf \n", x, f);

	return EXIT_SUCCESS;
}
