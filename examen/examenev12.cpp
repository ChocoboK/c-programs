

#include <stdio.h>
#include <stdlib.h>

int main(){
    int h;

    printf("Dime un numero hexadecimal: \n");
    scanf(" %x", &h);
    if (h %3 == 0)
        printf("Es multiplo de 3");
    else
        printf("No es multiplo de 3");

	return EXIT_SUCCESS;
}
