
#include <stdio.h>
#include <stdlib.h>

#define MAX 10
#define INC 0.5
double polinomio(double c[MAX], double x){
    double f = 0,
           potencia;

    for(int termino=0; termino<MAX; termino++){
        potencia = 1;
        for(int exponente=0; exponente<termino; exponente++)
            potencia *= x;
        f += c[termino] * potencia;
    }
    return f;
}

//Funcion punto de entrada
int main(){
    double f = 0, potencia, x, ultimo=0;

    double c[MAX] = {3, 1, 0, 0, 0, 0, 0, 0, 0, 0}; //las potencias a calcular

    printf("Limite inferior: ");
    scanf(" %lf", &x);

    /*
       1·x^0 + 2·x^1 + 3·x^2 + 4·x^3 + 5·x^4
       c[0]·x^0 + c[1]·x^1 + c[2]·x^2 + c[3]·x^3 + c[4]·x^4
       c[i]·x^i                                                  <--- este comentario es el calculo a "papel" del for de termino de abajo
       f += c[i]·x^i                                                  la i es termino en el for
       */

    for(double t=x; t == x || ultimo*f>0; t+=INC){
        ultimo = f;
        f= polinomio(c, t);
        if (t == x)
            ultimo = x;
        printf("f(%.2lf) = %.2lf \n", t, f);
        getchar();
    }
    return EXIT_SUCCESS;
}
