
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define M 2
#define K 4
#define N 3

//Funcion punto de entrada
int main(){
    int i, j;
    double A[M][K] = {
                      {2, 3, 5, 1},
                      {3, 1, 4, 2}
                     },
           B[K][N] = {
                      { 5,  2,  1},
                      { 3, -7,  2},
                      {-4,  5,  1},
                      { 2,  3, -9}
                     },
           C[M][N];

    //Recogida de datos
    //printf("Fila: ");
    //scanf(" %i", &i);
    //printf("Columna: ");
    //scanf(" %i", &j);

    for(int i=0; i<M; i++)
        for(int j=0; j<N; j++){
            C[i][j] = 0;
            for(int k=0; k<K; k++)
                C[i][j] += A[i][k] * B[k][j];
        }
    for(int i=0; i<M; i++){
        for(int j=0; j<N; j++)
            printf("\t %.0lf", C[i][j]);
        printf("\n");
    }

    return EXIT_SUCCESS;
}
