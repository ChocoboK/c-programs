
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INC 0.0001

//Evalua el valor de un polinomio del coeficiente en el punto x
double pol(double *coef, int grado, double x){
    int lon = (grado + 1) * sizeof (double);
    double *copia = (double *) malloc (lon);
    memcpy (copia, coef, lon);

    for(int i=0; i<grado; i++)
        for(int celda=0; celda<=i; celda++)
            *(copia + celda) *= x;

    for(int i=0; i<=grado; i++)
        resultado += *(copia + i);

    free(copia);
}

double integral (double li, double ls, double (*pf)(double) ){
    double area = 0;
    for (double x=li; x<ls; x+=INC)
        area += INC * (*pf)(x);

    return area;
}

double parabola (double x){
    double coef[]={1,0,0};
    return pol(coef, 2, x)
}
//Funcion punto de entrada
int main(int argc, char *argv[]){

    printf("El area es:\n%.2lf\n", integral(1, 3, &parabola));

    return EXIT_SUCCESS;
}
