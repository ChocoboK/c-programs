
#include <stdio.h>
#include <stdlib.h>

#define INC 0.0001

double parabola (double x){
    return x * x;
}

double integral (double li, double ls, double (*pf)(double) ){
    double area = 0;
    for (double x=li; x<ls; x+=INC)
        area += INC * (*pf)(x);
}

void imprimirR() {
    double x, y;

    system("clear");
    system("toilet -fpagga --filter metal AREA");
    printf("Dime el limite inferior: \n");
    scanf(" %lf", &x);
    printf("Dime el limite superior: \n");
    scanf(" %lf", &y);

    printf("El area es:\n%.2lf\n", integral(x, y, &parabola));
}

//Funcion punto de entrada
int main(int argc, char *argv[]){

    imprimirR();

    return EXIT_SUCCESS;
}
