
#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <strings.h>

#define MAX 0x100

// Estructura creada
struct TPila {
    int datos[MAX];
    int cima;
};

// Esta funcion metera los datos a la pila
void push (struct TPila *p, int nuevo){
    p->datos[p->cima] = nuevo;
    p->cima++;
}

// Esta funcion sacara los datos de la pila
int pop (struct TPila *p){
    int datos;
    p->cima--;
    datos = p->datos[p->cima];

    return datos;
}

// Imprimira los datos que le pongamos
void imprimir (struct TPila p){
    printf("\tPILA\n");
    printf("\t====\n");
    for (int i=0; i<p.cima; i++)
        printf("\t %i\n", p.datos[i]);
    printf("\n");
}

//Funcion punto de entrada
int main(int argc, char *argv[]){
    struct TPila pila;
    int devuelto = 0;
    int nuevo;
    int d;
    bool fin = false;
    bzero (&pila, sizeof(pila)); // llena de 0 la posicion de pila segun su tamaño

    //do - while que ejecutara las funciones
    do {
        system("clear");
        imprimir(pila);
        printf("Devuelto: %i\n", devuelto);
        printf("Entrada: ");
        d = scanf(" %i", &nuevo);
        __fpurge(stdin); // Limpia (purga) el tubo stdin
        if (d)
            push (&pila, nuevo);
        else
            devuelto = pop(&pila);
    } while (!fin);


    return EXIT_SUCCESS;
}
