
#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100
#define N 5

struct TE {
    char nombre[MAX];
    double sueldo;
};

//Funcion punto de entrada
int main(int argc, char *argv[]){
    struct TE empleado[N];

    for (int i=0; i<N; i++){
        printf("Nombre: ");
        scanf(" %s", empleado[i].nombre);
        printf("Sueldo: ");
        scanf(" %lf", &empleado[i].sueldo);
    }

    for (int i=0; i<N; i++){
        printf(" %s => %.2lf€ \n", empleado[i].nombre, empleado[i].sueldo);
    }
    return EXIT_SUCCESS;
}
