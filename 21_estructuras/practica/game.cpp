
#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <strings.h>


// Estructura vector
struct TVector {
    double x;
    double y;
    struct pos;
    struct vel;
};

// Estructura movil
struct TMovil {
    struct TVector pos;
    struct TVector vel;
    void(*mover)(struct TMovil *p);
};

void moverlento(struct TMovil *p){
    p->pos.x += p->vel.x+0.05;
    p->pos.y += p->vel.y+0.05;
}

//Funcion punto de entrada
int main(int argc, char *argv[]){
    struct TMovil *p = (struct TMovil *) malloc (sizeof(struct TMovil));

    do{
    printf("x");
    } while(system(TIME+1));


return EXIT_SUCCESS;
}
