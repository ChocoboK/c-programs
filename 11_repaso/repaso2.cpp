
#include <stdio.h>
#include <stdlib.h>

#define N 11

int main(){
    double nota[N];
    double media;

    //Entrada de datos
    for(int i=1; i<N; i++){
        printf("Que nota ha sacado el alumno %i: ", i);
        scanf(" %lf", &nota[i]);
        if(nota[i]<0)
            break;
    }

    //CALCULOS
    for(int i=1; i<N; i++)
        media += nota[i];
        media /= N;

    //Salida de datos
    printf("Nota media: %.2lf\n", media);

    return EXIT_SUCCESS;
}
