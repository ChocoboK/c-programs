
#include <stdio.h>
#include <stdlib.h>

int main(){
	int dn, mn, an;
	int dh, mh, ah;
	int dias;

	printf("Nacimiento: ");
	scanf(" %d/%d/%i", &dn, &mn, &an);	
	printf("Hoy: ");
	scanf(" %d/%d/%i", &dh, &mh, &ah);
	
	dias = dh - dn + (mh - mn) * 30 + (ah - an) * 365;
	printf("Has vivido %i dias \n", dias);

	return EXIT_SUCCESS;
}
