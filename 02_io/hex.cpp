
#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada /* tambien vale asi*/
int main(){
	int numero;
	
	printf("Dime un numero: \n");
	scanf("%i", &numero);
	printf("Tu numero hexadecimal es: 0x%x \n", numero);
	
	return EXIT_SUCCESS;
}
