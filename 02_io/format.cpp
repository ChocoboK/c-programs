#include <stdio.h>
#include <stdlib.h>

int main(){

    printf("%c", 97); //codigo ascii del caracter
    printf("%c", 0x61); //hexadecimal
    printf("%c", 'a'); //constante del caracter

    puts(NULL);
    printf("%i", 97); //esto imprimiria un 97 como tal 	

    /*
     4 => '4'
     4 + '0'
     4 + 0x30
     '4'
     '4' - '0' =>
     '7'
     4*10 => 40			
     '7' -'0' => 7
     40 + 7 = 47
     47 * 10 => 470
     '5' - '0' => 5
     470 + 5
     475			 	                
     */

    puts("");
    printf("%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n", 1,2,3,4,5,6,7,8,9,10,11,12,13,14);

    printf("\thola\n"); // \t para tabular, vim cambia el tabular por 4 espacios.

    int numero; // poner la variable aqui abajo es codigo c++, ya que en c las variables se deben escribir arriba.	
    printf("Num; ");
    scanf(" %i", &numero);
    printf("numero => [%p]: %i\n", &numero, numero); 
    /*%p sirve como puntero para apuntar a la memoria y este te imprime donde esta guardada esa variable en la direccion de memoria, esto es porque le hemos puesto &numero en argumentos */
    printf("numero => [%p]: %i\n", (void *) numero, numero); // void delante de numero es un molde ( lo daremos mas adelante en el tema correspondiente)
    printf"(" Linea %i\n", 47); // al no escribir la variable, printf mandara imprimir un 47.

    int dia, annio;	
    printf("Nacimiento dd/mm/aaaa: ");
    scanf("%i %*i %i", &dia, &annio); // * es un caracter de supresion de asignacion, esto serviria para que coja el numero y en vez de mandarlo a printf lo tiraria (no lo imprimiria). 

    return EXIT_SUCCESS;
}
