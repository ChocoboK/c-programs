
#include <stdio.h>
#include <stdlib.h>

int main(){
    int dia, mes, anio;
    int dia2, mes2, anio2;

    printf("Fecha de nacimiento (dd/mm/aaaa): ");
    scanf("%i/%i/%i", &dia, &mes, &anio);
    printf("Dime la fecha actual: ");
    scanf("%i/%i/%i", &dia2, &mes2, &anio2);
    printf("Has vivido %i dias, %i meses y %i años \n", dia - dia2 * 365, mes - mes2 * 30, anio2 - anio);
    printf("Has vivido %i dias", (dia2 - dia) + (mes2 - mes * 30) + (anio2 - anio * 365) ); 
    
    char hex[32];
    scanf(" %[0-9a-fA-F]", hex); //esto quiere decir saca del tubo, numeros de 0 a 9 y letras de la aA a la fF, si pones %[^ sera un conjunto de seleccion inverso (que no sacara eso)

    return EXIT_SUCCESS;
}
