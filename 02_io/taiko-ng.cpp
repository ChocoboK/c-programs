
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 10
#define BTU 100000 /*Basic Time Unit*/ 

int main(){
    int duracion[VECES] = { 0, 2, 2, 6, 2, 6, 2, 1, 4, 1 };
     
    //scanf("%i", &duracion);
    /* bucle for: debe valer para repetir cosas*/
    for (int i=0; i<VECES; i++) {
	fputc('\a', stderr);
	usleep(duracion[i] * BTU);
    }

    return EXIT_SUCCESS;
}
