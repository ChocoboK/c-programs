
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(){
//printf esta bufferizado, por eso necesita un \n, stderr no esta bufferizado.	//fprintf imprime caracteres en el tubo que yo le diga, como stderr.		
//printf("hola %s, que tal estas? %i\n", "pepe", (int) '0'); dara 48 en el %i

	fprintf(stderr,"\a");
	sleep(1);	
	fputc('\a', stderr);
	usleep(100000);
	printf("\a\n");
	sleep(2);	
	printf("\a\n");
	usleep(100000);
	printf("\a\n");

	return EXIT_SUCCESS;
}
