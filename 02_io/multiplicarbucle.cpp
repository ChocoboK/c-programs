
#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

void pon_titulo(int tabla){//esto seria un parametro formal
    //poner numeros 0x en [] de un char, es hardcodear, por eso es mejor usar un define.
    char titulo[MAX];
    //Titulo, poner el void es declarar una funcion. Luego se le dice al compilador la funcion que hace.
    sprintf(titulo, "toilet -fpagga --metal Tabla del %i", tabla);
    system(titulo);
}

int main(){
    //Declaracion de variables
    int i,tabla,mult;
   
    //Menu
    printf("Escribe el numero a multiplicar: ");
    scanf("%i", &tabla); 
    
    //Esto seria una llamada de la funcion indicada en void.
    pon_titulo(tabla);
    /*llamada con parametro actual es cuando tienen un numero, porque le oblgia, ej: x(3); */ 

    //Resultados
    for(i=1;i<=10;i++)
    {
	mult=tabla*i;
	printf("%i x %i = %i \n", tabla,i,mult);
    }
    return EXIT_SUCCESS;
}
