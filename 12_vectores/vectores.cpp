
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    double x1,
           x2,
           y1,
           y2,
           resultado;
    double cos;

    printf("Dime los numeros para el primer vector (X,Y): ");
    scanf(" %lf,%lf", &x1, &y1);
    printf("Dime los numeros para el segundo vector (X,Y): ");
    scanf(" %lf,%lf", &x2, &y2);

    resultado = (x1 * x2) + (y1 * y2);
    cos = resultado/ (sqrt( pow(x1,2) + pow(y1,2)) * sqrt( pow(x2,2) + pow(y2,2)));

    printf("Este es el resultado de los vectores (%.0lf,%.0lf) y (%.0lf,%.0lf): %.0lf \n", x1 , y1, x2, y2, resultado);
    printf("Coseno: %.4lf \n", cos);
    printf("Angulo: %.2lf \n", acos((resultado / cos) * 180/M_PI));
    //printf("Angulo: %.2lf \n", )

    return EXIT_SUCCESS;
}
