
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.14

int main(){
    double x, y;
    int ang;
    double r00, r01, r10, r11;
    double resultado, resultado2;

    printf("Dime las coordenadas (X Y): \n");
    scanf("%lf %lf", &x, &y);
    printf("Dime un angulo a rotar: \n");
    scanf("%i", &ang);
    printf(" \n");

    r00 = cos( ang * PI / 180);
    r01 = sin( ang * PI / 180);
    r10 = sin( ang * PI / 180);
    r11 = cos( ang * PI / 180);

    resultado  = (x * r00) - (y * r01);
    resultado2 = (x * r10) + (y * r11);

    printf("Este es el resultado: %.2lf %.2lf \n", resultado, resultado2);

	return EXIT_SUCCESS;
}
