
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    double x1,
           x2,
           y1,
           y2,
           resultado;

    printf("Dime los numeros para el primer vector (X,Y): ");
    scanf(" %lf,%lf", &x1, &y1);
    printf("Dime los numeros para el segundo vector (X,Y): ");
    scanf(" %lf,%lf", &x2, &y2);

    resultado = (x1 * y2 - x2 * y1);

    printf("Producto vectores: 0,0,%.0lf \n", resultado);

        return EXIT_SUCCESS;
}
