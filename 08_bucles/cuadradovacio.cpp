
#include <stdio.h>
#include <stdlib.h>

int main(){

    int l = 5;

    for(int fila=1; fila<=l; fila++){
        for(int col=1; col<=l; col++)
            if(fila==1 || col==1){
                printf(" *");
            }
            else{
                if(fila==l || col==l)
                    printf(" *");
                else
                    printf("  ");
            }
        printf("\n");
    }
    printf("\n");
    return EXIT_SUCCESS;
}
