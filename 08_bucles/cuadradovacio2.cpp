#include <stdio.h>
#include <stdlib.h>

int main(){

    int l;

    printf("Dime un numero: ");
    scanf(" %i", &l);

    printf("\n");
    for(int fila=0; fila<l; fila++){
        for(int col=0; col<l; col++)
            if(fila==0 || col==0 || fila==l-1 ||col==l-1 || col==fila || col+fila==l-1)
                printf(" *");
            else
                printf("  ");
        printf("\n");
    }
    printf("\n");
    return EXIT_SUCCESS;
}
