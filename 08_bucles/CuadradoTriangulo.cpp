
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

int main(){
    int num;
    int elegido;

    system("clear");
    printf("Cuadrado (1) Triangulo (2): ");
    scanf(" %i", &elegido);

    switch(elegido){
        case(1):
            printf("Tamaño del cuadrado: ");
            scanf(" %i", &num);
            system("clear");
            for (int fila=0; fila<num; fila++){
                for (int col=0; col<num; col++)
                    printf(" *");
                printf(" \n");
            }
            break;
        case(2):
            printf("Tamaño del triangulo: ");
            scanf(" %i", &num);
            system("clear");
            for (int fila=0; fila<num; fila++){
                for (int col=0; col<=fila; col++)
                    //for (int c=0;c<-fila;c++)
                        printf(" ");
                    printf(" *");
                printf(" \n");
            }
            break;
        default:
            printf("Buen intento, maquinola \n");
            break;
    }
    return EXIT_SUCCESS;
}
