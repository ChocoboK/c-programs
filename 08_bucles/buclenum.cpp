#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//#define N 361

//#define GRADOS

#ifdef GRADOS
#define K 1
const char *unidad = "º";
#else
#define K M_PI / 180
const char *unidad = "rad";
#endif

int main(){
    //int i; (int indicado en el for)

    for (double grados=0; grados<K*360; grados+=K*.5)
        printf(" %.2lf\n", grados);

    /*    for(int i=0; i<N; i++)
          printf("Numero %i \n", i);
     */
    return EXIT_SUCCESS;
}
