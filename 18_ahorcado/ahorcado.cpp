
#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#define GREEN "\x1B[32m;"
#define RED "\x1B[31m;"
#define CYAN "\x1B[36m;"
#define YELLOW "\x1B[33m;"
#define RESET "\x1B[0m;"
#define N 0x100
#define VIDA 6

bool es_mala (char letra) {
    return true;
}

//Funcion punto de entrada
int main(int argc, char *argv[]){

    char letra;
    char *malas, *letras;
    int total_malas = 0, total_letras = 0;

    malas = (char *) malloc (VIDA+1);
    letras = (char *) malloc (N+1);
    bzero(malas, VIDA+1);
    bzero(letras, N+1);

    while (total_malas < VIDA) {
        printf( CYAN "Letras: " RESET GREEN "%s" RESET "\n", letras);
        printf( CYAN "Malas: " RESET RED "%s" RESET "\n", malas);
        printf("~~~~~~~~~~~~~~~~~~~~~ \n");

        printf( YELLOW "Letra: " RESET );
        letra = getchar();
        __fpurge(stdin);

        if (strchr(letras, letra))
            continue;

        if (es_mala(letra))
            *(malas + total_malas++) = letra;
        *(letras + total_letras++) = letra;
    }


    free(letras);
    free(malas);

    return EXIT_SUCCESS;
}
