
#include <stdio.h>
#include <stdlib.h>

#define N 13
#define VECES 20

int main(){
    int A[] = {5, 20, 3, 9, 2, 13, 17, 9};
    int pos = -1;
    int buscado = N;

    /* Busca el 13 en A */
    //printf("Dime un numero a buscar: ");
    //scanf(" %i", &buscado);

    for(int busca=0; busca<VECES; busca++) // puedes poner busca<sizeof(A)/sizeof(int), esto ocupara en numero de bytes, en este caso sizeof(A) son: 4bytes (36 elementos) divide
        if(A[busca] == buscado){
            printf(" En la posicion %i esta el %i \n", busca+1, A[busca]);
            pos = busca;
            printf(" Posicion real: %i \n", pos);
        }

    return EXIT_SUCCESS;
}
