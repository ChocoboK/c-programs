

#include <stdio.h>
#include <stdlib.h>

#define N 13
#define VECES 20
int encuentra(int buscado, int A[]){
    int pos = -1;

    for(int busca=0; busca<sizeof(A)/sizeof(int); busca++)
        if(A[busca] == buscado)
            pos = busca;
    return pos;
}

int main(){
    int A[] = {5, 20, 3, 9, 2, 13, 17, 9};
    int buscado = N;
    int pos = encuentra(buscado, A, sizeof(A)/sizeof(int));
    if (pos > 0)
        printf("%i esta en %i", buscado, pos);
    else
        printf("No ta!");

    return EXIT_SUCCESS;
}
